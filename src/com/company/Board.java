package com.company;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Graphics2D;
import java.util.List;
import java.util.ArrayList;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.Math;

import java.awt.Rectangle;

import java.awt.Font;
import java.awt.FontMetrics;

public class Board extends JPanel implements Runnable{

    private final static int DELAY = 10;
    private List<Alien> aliens= new ArrayList<>();
    private boolean ingame;

    private Frog frog;

    Board (){
        initBoard();
    }

    private void initBoard() {
        ingame=true;
        addKeyListener(new TAdapter());
        setBackground(Color.BLACK);
        setFocusable(true);
        setPreferredSize(new Dimension(Constants.WIDTH,Constants.HEIGHT));
        frog = new Frog(400,500);

    }
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (ingame) {

            drawObjects(g);

        } else {

            drawGameOver(g);
        }
        Toolkit.getDefaultToolkit().sync();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        Thread animator = new Thread(this);
        animator.start();
    }

    private void drawObjects(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        g2d.drawImage(frog.getImage(), frog.getX(), frog.getY(), this);

        List<Missile> missiles = frog.getMissiles();

        for (Missile missile : missiles) {
            g2d.drawImage(missile.getImage(), missile.getX(), missile.getY(), this);
        }
        for (Alien alien : aliens) {
            if (alien.isVisible()) {
                g.drawImage(alien.getImage(), alien.getX(), alien.getY(), this);
            }
        }
        g.setColor(Color.WHITE);
        g.drawString("Aliens left: " + aliens.size(), 5, 15);

    }

    private void drawGameOver(Graphics g) {

        String msg = "Game Over";
        Font small = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics fm = getFontMetrics(small);

        g.setColor(Color.white);
        g.setFont(small);
        g.drawString(msg, (Constants.WIDTH - fm.stringWidth(msg)) / 2,Constants.HEIGHT / 2);
    }

    private void step() {

        updateMissiles();
        updateSpaceShip();
        updateAliens();
        checkCollisions();
        repaint();
    }

    private void updateAliens() {
        if((Math.random()*100)>99&&(aliens.size()<5)) {
            aliens.add(new Alien((int)(Math.random()*(Constants.WIDTH-100)),0));
        }

        for (int i = 0; i < aliens.size(); i++) {
            Alien alien=aliens.get(i);
            if (alien.isVisible()) {

                alien.move();
            } else {

                aliens.remove(i);
            }
        }

    }

    private void updateMissiles() {

        List<Missile> missiles = frog.getMissiles();

        for (int i = 0; i < missiles.size(); i++) {

            Missile missile = missiles.get(i);

            if (missile.isVisible()) {

                missile.move();
            } else {

                missiles.remove(i);
            }
        }
    }

    public void checkCollisions() {

        Rectangle r3 = frog.getBounds();

        for (Alien alien : aliens) {

            Rectangle r2 = alien.getBounds();

            if (r3.intersects(r2)) {

                frog.setVisible(false);
                alien.setVisible(false);
                ingame = false;
            }
        }

        List<Missile> ms = frog.getMissiles();

        for (Missile m : ms) {

            Rectangle r1 = m.getBounds();

            for (Alien alien : aliens) {

                Rectangle r2 = alien.getBounds();

                if (r1.intersects(r2)) {

                    m.setVisible(false);
                    alien.setVisible(false);
                }
            }
        }
    }

    private void updateSpaceShip() {

        frog.move();
    }

    private class TAdapter extends KeyAdapter {

        @Override
        public void keyReleased(KeyEvent e) {
            frog.keyReleased(e);
        }

        @Override
        public void keyPressed(KeyEvent e) {
            frog.keyPressed(e);
        }
    }
    @Override
    public void run() {

        long beforeTime, timeDiff, sleep;

        beforeTime = System.currentTimeMillis();

        while (true) {

            if(!ingame) break;
            step();

            timeDiff = System.currentTimeMillis() - beforeTime;
            sleep = DELAY - timeDiff;

            if (sleep < 0) {
                sleep = 2;
            }

            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {

                String msg = String.format("Thread interrupted: %s", e.getMessage());

                JOptionPane.showMessageDialog(this, msg, "Error",
                        JOptionPane.ERROR_MESSAGE);
            }

            beforeTime = System.currentTimeMillis();
        }
    }
}