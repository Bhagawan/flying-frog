package com.company;

public class Missile extends Sprite {

    private final int BOARD_H = 600;
    private final int MISSILE_SPEED = 2;

    public Missile(int x, int y) {
        super(x, y);

        initMissile();
    }

    private void initMissile() {

        loadImage("src/resources/spell.png");
        getImageDimensions();
    }

    public void move() {

        y -= MISSILE_SPEED;

        if (x < 0) {
            visible = false;
        }
    }
}