package com.company;

import java.awt.Image;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

class Frog extends Sprite{
    private int dx;
    private int dy;
    private List<Missile> missiles;

    Frog(int x, int y) {
        super(x,y);
        initFrog();
    }

    private void initFrog() {
        missiles = new ArrayList<>();
        loadImage("src/resources/frog.png");
        image = image.getScaledInstance(100,100,Image.SCALE_DEFAULT);
        //getImageDimensions();
        width=100;
        height=100;
    }

    void move() {
        x += dx;
        y += dy;

        if (x < 1) {
            x = 1;
        }
        else if(x>Constants.WIDTH-100){
            x=Constants.WIDTH-100;
        }
        if (y < 1) {
            y = 1;
        }
        else if(y>Constants.HEIGHT-100){
            y=Constants.HEIGHT-100;
        }
    }

    public List<Missile> getMissiles() {
        return missiles;
    }

    public void fire() {
        missiles.add(new Missile(x + 50, y ));
    }

    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_SPACE) {
            fire();
        }

        if (key == KeyEvent.VK_LEFT) {
            dx = -6;
        }

        if (key == KeyEvent.VK_RIGHT) {
            dx = 6;
        }

        if (key == KeyEvent.VK_UP) {
            dy = -6;
        }

        if (key == KeyEvent.VK_DOWN) {
            dy = 6;
        }
    }

    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT) {
            dx = 0;
        }

        if (key == KeyEvent.VK_RIGHT) {
            dx = 0;
        }

        if (key == KeyEvent.VK_UP) {
            dy = 0;
        }

        if (key == KeyEvent.VK_DOWN) {
            dy = 0;
        }
    }

}
