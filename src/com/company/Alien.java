package com.company;

public class Alien extends Sprite {


    public Alien(int x, int y) {
        super(x, y);

        initAlien();
    }

    private void initAlien() {

        loadImage("src/resources/alien.png");
        getImageDimensions();
    }

    public void move() {
        y += 1;
        if(y>Constants.HEIGHT) visible=false;
    }
}