package com.company;
import java.awt.*;
import javax.swing.JFrame;

public class Application extends JFrame {


    private Application() {

        initUI();
    }

    private void initUI() {

        add(new Board());
        setResizable(false);
        pack();

        setTitle("Application");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

    }

    public static void main(String[] args) {

        EventQueue.invokeLater(() -> {
            Application ex = new Application();
            ex.setLocationRelativeTo(null);
            ex.setVisible(true);
        });
    }
}